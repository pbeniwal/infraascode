**Jenkins job to execute the script**


![DATE](GKE.JPG)



**If running directly below commands can be used:**

./script.sh

cat filename.txt | xargs curl -o HelloWorld.war

docker build -t pbeniwal/helloworld:v$BUILD_ID .

docker push pbeniwal/helloworld:v$BUILD_ID

kubectl delete deploy helloworld | echo 1 

kubectl create deploy helloworld --image=pbeniwal/helloworld:v$BUILD_ID --port=8080

kubectl expose deployment helloworld --type LoadBalancer --port 80 --target-port 8080 | echo 1 

